﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FastagRecharge.aspx.cs" Inherits="FastagRecharge" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fastag Recharge:</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
     <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<body>
    <form id="FRecharge" runat="server">
        <div class="container">
            <br />
            <div class="row" align="center">
                <div class="col-sm-12">
                    <asp:Label ID="lblFRTitle" runat="server" Text="Fastag Recharge" Font-Bold="true" Font-Underline="true"></asp:Label>
                </div>
                <br />
                <br />
            </div>
            <div class="row">
                 <div class="col-sm-4"></div>
                 <div class="col-sm-4" align="center">
                    <asp:Label ID="lblFasTagID" Font-Bold="true" runat="server">Enter FasTag ID: </asp:Label>
                    <asp:TextBox ID="txtFasTagID" runat="server" PlaceHolder="Enter FasTag ID" />
                </div>
                 <div class="col-sm-4"></div>
            </div><br /><br />
             <div class="row">
                 <div class="col-sm-4"></div>
                <div class="col-sm-4" align="center">
                    <asp:Label ID="lblFasRechAmont" Font-Bold="true" runat="server">Recharge Amount: </asp:Label>
                        <asp:DropDownList ID="ddlFasRechAmont" runat="server">
                        <asp:ListItem>Select Recharge Amount</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>100</asp:ListItem>
                        <asp:ListItem>200</asp:ListItem>
                        <asp:ListItem>500</asp:ListItem>
                        <asp:ListItem>1000</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-4"></div>
            </div>
            <br /><br/><br /><br /><br />
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4" align="center">
                    <asp:Button ID="btnFasTagRecharge" Text="Recharge" runat="server" CssClass="btn btn-success btn-primary" 
                        OnClick="btnFasTagRecharge_Click"/>
                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="btn btn-danger btn-primary" PostBackUrl="~/Fastag_Registration.aspx"/>
                    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CssClass="btn btn-warning btn-primary" />
                    </div> 
                <div class="col-sm-4"></div>
            </div>
            <br />
            <div class="box-body table-responsive">
                <asp:GridView ID="gvFasTag" Align="Center" HeaderStyle-BackColor="OliveDrab" HeaderStyle-BorderColor="#6600cc" HeaderStyle-ForeColor="#ffcc99" runat="server" 
                    AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" CssClass="table table-bordered table-striped gvfilters"
                    EmptyDataText="No records has been added.">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No">
                            <ItemTemplate>
                                <asp:Label ID="lblSINO" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fastag ID">
                            <ItemTemplate>
                                <asp:Label ID="lblFastagID" runat="server" Text='<%#ProcessMyDataItem(Eval("NVAR_FASTAG_ID")) %>'></asp:Label>
                            </ItemTemplate>
                         </asp:TemplateField>
                        <asp:TemplateField HeaderText="Owner Name">
                            <ItemTemplate>
                             <asp:Label runat="server" ID="lblOwnerName" Text='<%#ProcessMyDataItem(Eval("NVAR_OWNER_NAME")) %>'></asp:Label>
                            </ItemTemplate>                           
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vehicle Reg No">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblVehicleRegNo" Text='<%#ProcessMyDataItem(Eval("NVAR_VEHICLE_REG_NO")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vehicle Type">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblVehicleType" Text='<%#ProcessMyDataItem(Eval("CHAR_VEHICLE_TYPE")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Wallet Balance">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblWalletBalance" Text='<%#ProcessMyDataItem(Eval("INT_WALLET")) %>'></asp:Label>
                            </ItemTemplate>
                         </asp:TemplateField>                        
                      </Columns>
                </asp:GridView>
            </div>
        </div>
    </form>
</body>
</html>
