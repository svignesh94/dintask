﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Data.OleDb;

public partial class Fastag_Registration : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
    DataProvider dp = new DataProvider();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs ex)
    {
        try
        {
            if (!IsPostBack)
            {
                txtFID.Focus();
                LoadMainGrid();
                if (gvFasTag.DataSource == null)
                {
                    gvFasTag.DataSource = new string[] { };
                }
                gvFasTag.DataBind();
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public string ProcessMyDataItem(object myValue)
    {
        if (myValue.ToString() == "")
        {
            return "-";
        }
        return myValue.ToString();
    }
    protected void DisplayToastr(string message, string type)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "toastr", "alertMe(" + "\"" + message + "\"" + "," + "\"" + type + "\"" + ")", true);
    }
    protected void btnAdd_Click(object sender, EventArgs ex)
    {
        try
        {
            if (txtFID.Text != "" && txtOwnerName.Text != "" && txtVehicleRegNo.Text != "" && rblVehicleType.Text != "")
            {
                BLL objFastagBLL = new BLL();

                objFastagBLL.FastagID = txtFID.Text.ToUpper().Trim();
                objFastagBLL.OwnerName = txtOwnerName.Text.ToUpper().Trim();
                objFastagBLL.VehicleRegNo = txtVehicleRegNo.Text.ToUpper().Trim();
                objFastagBLL.VehicleType = rblVehicleType.Text.ToUpper().Trim();
                objFastagBLL.Logtime = DateTime.Now;
                
                BLP objFastagBLP = new BLP();
                objFastagBLP.InsertFastagDetails(objFastagBLL);
                dp.ClearTextBoxes(this.Page);
                LoadMainGrid();
                DisplayToastr("Saved Successfully", "Success");
                txtFID.Focus();
            }
            else
            {
                DisplayToastr("Enter All Mandatory Fields", "Warning");
            }
        }


        catch (Exception e)
        {
            throw e;

        }
    }
    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        gvFasTag.EditIndex = e.NewEditIndex;
        this.LoadMainGrid();
    }
    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        BLL objFastagBLL = new BLL();
        GridViewRow row = gvFasTag.Rows[e.RowIndex];
        objFastagBLL.ID = Convert.ToInt32(gvFasTag.DataKeys[e.RowIndex].Values[0]);
        objFastagBLL.FastagID = (row.FindControl("txtEditFastagID") as TextBox).Text;
        objFastagBLL.OwnerName = (row.FindControl("txtEditOwnerName") as TextBox).Text;
        objFastagBLL.VehicleRegNo = (row.FindControl("txtEditVehicleRegNo") as TextBox).Text;
        objFastagBLL.VehicleType = (row.FindControl("txtEditVehicleType") as TextBox).Text;
        objFastagBLL.WalletAmount = Convert.ToDecimal((row.FindControl("txtEditWalletBalance") as TextBox).Text);
        objFastagBLL.Logtime = DateTime.Now;
       
        BLP objFastagBLP = new BLP();
        objFastagBLP.UpdateFastagDetails(objFastagBLL);
        dp.ClearTextBoxes(this.Page);
        DisplayToastr("Saved Successfully", "Success");
        txtFID.Focus();
        gvFasTag.EditIndex = -1;
        this.LoadMainGrid();
    }
    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        gvFasTag.EditIndex = -1;
        this.LoadMainGrid();
    }
    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        BLL objFastagBLL = new BLL();
        objFastagBLL.ID = Convert.ToInt32(gvFasTag.DataKeys[e.RowIndex].Values[0]);
        BLP objFastagBLP = new BLP();
        objFastagBLP.DeleteFastagDetails(objFastagBLL);
        DisplayToastr("Deleted Successfully", "Delete");
        txtFID.Focus();
        this.LoadMainGrid();
    }

    protected void btnClear_Click(object sender, EventArgs ex)
    {
        txtFID.Text = string.Empty;
        txtOwnerName.Text = string.Empty;
        txtVehicleRegNo.Text = string.Empty;
        rblVehicleType.ClearSelection();
    }

    public void LoadMainGrid()
    {
        ds.Clear();
        string q = "select * from Fastage_Customer_Details where CHAR_STATUS='A' ORDER BY NVAR_OWNER_NAME ASC";
        ds = dp.GetTableSet(q);
        gvFasTag.DataSource = ds;
        gvFasTag.DataBind();
    }
    protected void btnActive_Click(object sender, EventArgs e)
    {
       GridViewRow grows = (GridViewRow)(sender as Control).Parent.Parent;
        int rowIndex = grows.RowIndex;
      //get grid row index
       Label ID = (Label)gvFasTag.Rows[rowIndex].FindControl("lblID");
        BLL objFastagBLL = new BLL();
        string idUpdate = "update Fastage_Customer_Details SET CHAR_RECORDSTATUS='A' WHERE ID=@id";
        con.Open();
        SqlCommand cmd = new SqlCommand(idUpdate, con);
        cmd.Parameters.AddWithValue("@ID", ID.Text);
        cmd.ExecuteNonQuery();
        con.Close();
        this.LoadMainGrid();
    }

    protected void btnInActive_Click(object sender, EventArgs e)
    {
        GridViewRow grows = (GridViewRow)(sender as Control).Parent.Parent;
        int rowIndex = grows.RowIndex;
        //get grid row index
        Label ID = (Label)gvFasTag.Rows[rowIndex].FindControl("lblID");
        BLL objFastagBLL = new BLL();
        string idUpdate = "update Fastage_Customer_Details SET CHAR_RECORDSTATUS='I' WHERE ID=@id";
        con.Open();
        SqlCommand cmd = new SqlCommand(idUpdate, con);
        cmd.Parameters.AddWithValue("@ID", ID.Text);
        cmd.ExecuteNonQuery();
        con.Close();
        this.LoadMainGrid();
    }
    protected void gvFasTag_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label rstatus = (Label)e.Row.FindControl("lblRtatus");
            Button aActive = (Button)e.Row.FindControl("btnActive");
            Button aInActive = (Button)e.Row.FindControl("btnInActive");

            if (rstatus.Text == "A")
            {
                aActive.Visible = false;
                aInActive.Visible = true;
                e.Row.BackColor = Color.White;
            }
            else if (rstatus.Text == "I")
            {
                aActive.Visible = true;
                aInActive.Visible = false;
                e.Row.BackColor = Color.Red;
            }
            //if to get string value use string directly 
            // if to get label value use label.text
        }

    }
}