﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;

/// <summary>
/// Summary description for BLL
/// </summary>
public class BLL
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
    public BLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    private string _FastagID, _OwnerName, _VehicleRegNo, _VehicleType, _UserName, _DefaultUserName;
    private decimal _WalletAmount, _AmountDetection, _RechargeAmount;
    private DateTime _Logtime;
    private int? _DefaultUser, _ID;
    public string FastagID
    {
        get { return _FastagID; }
        set { _FastagID = value; }
    }
    public string OwnerName
    {
        get { return _OwnerName; }
        set { _OwnerName = value; }
    }
    public string VehicleRegNo
    {
        get { return _VehicleRegNo; }
        set { _VehicleRegNo = value; }
    }
    public string VehicleType
    {
        get { return _VehicleType; }
        set { _VehicleType = value; }
    }
    public string UserName
    {
        get { return _UserName; }
        set { _UserName = value; }
    }
    public string DefaultUserName
    {
        get { return _DefaultUserName; }
        set { _DefaultUserName = value; }
    }

    public decimal WalletAmount
    {
        get { return _WalletAmount; }
        set { _WalletAmount = value; }
    }
    public decimal AmountDetection
    {
        get { return _AmountDetection; }
        set { _AmountDetection = value; }
    }
    public DateTime Logtime
    {
        get { return _Logtime; }
        set { _Logtime = value; }
    }
    public int? DefaultUser
    {
        get { return _DefaultUser; }
        set { _DefaultUser = value; }
    }

    public int? ID { get => _ID; set => _ID = value; }
    public decimal RechargeAmount { get => _RechargeAmount; set => _RechargeAmount = value; }
}