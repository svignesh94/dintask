﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
/// <summary>
/// Summary description for BLP
/// </summary>
public class BLP
{
    public BLP()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void InsertFastagDetails(BLL objBLLDetails)
    {
        DAL objFastagDAL = new DAL();
        try
        {
            objFastagDAL.InsertFastagDAL(objBLLDetails);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objFastagDAL = null;
        }
    }
    public void UpdateFastagDetails(BLL objBLLDetails)
    {
        DAL objFastagDAL = new DAL();
        try
        {
            objFastagDAL.UpdateFastagDAL(objBLLDetails);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objFastagDAL = null;
        }
    }
    public void RechargeFastagDetails(BLL objBLLDetails)
    {
        DAL objFastagDAL = new DAL();
        try
        {
            objFastagDAL.RechargeFastagDAL(objBLLDetails);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objFastagDAL = null;
        }
    }

    public void DeleteFastagDetails(BLL objBLLDetails)
    {
        DAL objFastagDAL = new DAL();
        try
        {
            objFastagDAL.DeleteFastagDAL(objBLLDetails);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objFastagDAL = null;
        }
    }
}