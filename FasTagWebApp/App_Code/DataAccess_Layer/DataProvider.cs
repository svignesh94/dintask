﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;
using System.IO;
using System.Security.Cryptography;
public class DataProvider
{
    private SqlConnection con;
    public SqlConnection sconn;

    private SqlDataAdapter da = new SqlDataAdapter();
    //private SqlDataReader dr;
    private SqlCommand cmd;
    private DataTable dt;
    private DataSet ds;
    //private static DataProvider provider;
    private static object threadlock = new object();
    public DataProvider()
    {
    }
    public SqlConnection GetConnection()
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["conString"].ToString();
        return new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
    }
    public bool Execute(string query)
    {
        bool result = false;
        lock (threadlock)
        {
            try
            {


                con = GetConnection();
                con.Open();
                cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception e)
            {
                result = false;
                throw e;

            }
            finally
            {
                cmd.Dispose();
                con.Close();

            }
        }
        return result;
    }
    public void ClearTextBoxes(Control p1)   // Clear all textbox
    {
        foreach (Control ctrl in p1.Controls)
        {
            if (ctrl is TextBox)
            {
                TextBox t = ctrl as TextBox;

                if (t != null)
                {
                    t.Text = String.Empty;
                }
            }
            else if (ctrl is DropDownList)
            {
                DropDownList t = ctrl as DropDownList;

                if (t != null)
                {
                    t.ClearSelection();
                }
            }
            else
            {
                if (ctrl.Controls.Count > 0)
                {
                    ClearTextBoxes(ctrl);
                }
            }
        }
    }

    public string GetColumnValue(string TableName, string ColumnName, string ColumnId, string Idvalue)
    {
        string value;
        string q = "select top(1) " + ColumnName + " from " + TableName + " WHERE " + ColumnId + "='" + Idvalue + "' AND FLDRECORDSTATUS='A'";


        dt = GetTable(q);
        if (dt.Rows.Count > 0)
        {
            value = dt.Rows[0][ColumnName].ToString();
        }
        else
        {
            value = null;
        }
        return value;
    }

    public DateTime? IsDateNull(string date)
    {
        DateTime? ConvDate;
        if (date != "")
        {
            ConvDate = DateTime.ParseExact(date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        }
        else
        {
            ConvDate = null;
        }

        return ConvDate;
    }


    public DataTable GetTable(string query)
    {
        dt = new DataTable();
        lock (threadlock)
        {
            try
            {
                con = GetConnection();
                con.Open();
                da = new SqlDataAdapter(query, con);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
            finally
            {
                da.Dispose();
                dt.Dispose();
                con.Close();

            }
        }
        return dt;
    }

    public DataSet GetTableSet(string query)
    {
        ds = new DataSet();
        lock (threadlock)
        {
            try
            {
                con = GetConnection();
                con.Open();
                da = new SqlDataAdapter(query, con);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
            finally
            {
                da.Dispose();
                ds.Dispose();
                con.Close();

            }
        }
        return ds;
    }

}