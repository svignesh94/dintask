﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;

/// <summary>
/// Summary description for DAL
/// </summary>
public class DAL
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
    public DAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void InsertFastagDAL(BLL objBLL)
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("sp_Fastag_Details", con);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@NVAR_FASTAG_ID", objBLL.FastagID);
            cmd.Parameters.AddWithValue("@NVAR_OWNER_NAME", objBLL.OwnerName);
            cmd.Parameters.AddWithValue("@NVAR_VEHICLE_REG_NO", objBLL.VehicleRegNo);
            cmd.Parameters.AddWithValue("@CHAR_VEHICLE_TYPE", objBLL.VehicleType);
            cmd.Parameters.AddWithValue("@DATETIME_LOG_TIME", objBLL.Logtime);
            cmd.Parameters.AddWithValue("@comment_name", "insert");
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;

        }
        finally
        {
            cmd.Dispose();
            con.Close();
        }

    }
    public void UpdateFastagDAL(BLL objBLL)
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("sp_Fastag_Details", con);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@ID", objBLL.ID);
            cmd.Parameters.AddWithValue("@NVAR_FASTAG_ID", objBLL.FastagID);
            cmd.Parameters.AddWithValue("@NVAR_OWNER_NAME", objBLL.OwnerName);
            cmd.Parameters.AddWithValue("@NVAR_VEHICLE_REG_NO", objBLL.VehicleRegNo);
            cmd.Parameters.AddWithValue("@CHAR_VEHICLE_TYPE", objBLL.VehicleType);
            cmd.Parameters.AddWithValue("@INT_WALLET", objBLL.WalletAmount);
            cmd.Parameters.AddWithValue("@DATETIME_LOG_TIME", objBLL.Logtime);
            cmd.Parameters.AddWithValue("@comment_name", "update");
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;

        }
        finally
        {
            cmd.Dispose();
            con.Close();
        }

    }
    public void RechargeFastagDAL(BLL objBLL)
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("sp_Fastag_Details", con);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {           
            cmd.Parameters.AddWithValue("@NVAR_FASTAG_ID", objBLL.FastagID);
            cmd.Parameters.AddWithValue("@INT_WALLET", objBLL.RechargeAmount);
            cmd.Parameters.AddWithValue("@DATETIME_LOG_TIME", objBLL.Logtime);
            cmd.Parameters.AddWithValue("@comment_name", "recharge");
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;

        }
        finally
        {
            cmd.Dispose();
            con.Close();
        }
    }
    public void DeleteFastagDAL(BLL objBLL)
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("sp_Fastag_Details", con);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.AddWithValue("@ID", objBLL.ID);
            cmd.Parameters.AddWithValue("@comment_name", "delete");
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch(Exception ex)
        {
            throw ex;
        }
        finally
        {
            cmd.Dispose();
            con.Close();
        }

    }
}