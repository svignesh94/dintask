﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Fastag_Registration.aspx.cs" Inherits="Fastag_Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fastag Registration</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <style type="text/css">
        .GridViewEditRow input[type=text] {
            width: 120px;
        }
        /* size textboxes */
        /*.GridViewEditRow select {width:150px;}*/ /* size drop down lists */
    </style>

</head>
<body>
    <form id="fFasTag" runat="server">
        <div class="container">
            <br />
            <div class="row" align="center">
                <div class="col-sm-12">
                    <asp:Label ID="lblTitle" runat="server" Text="Fastag Registration" Font-Bold="true" Font-Underline="true"></asp:Label>
                </div>
                <br />
                <br />
            </div>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <asp:Label ID="lblFID" Font-Bold="true" runat="server">Fastag ID: </asp:Label>
                    <asp:TextBox ID="txtFID" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-4">
                    <asp:Label ID="lblOwnerName" Font-Bold="true" runat="server">Owner Name: </asp:Label>
                    <asp:TextBox ID="txtOwnerName" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <asp:Label ID="lblVehicleRegNo" Font-Bold="true" runat="server">Vehicle Reg No: </asp:Label>
                    <asp:TextBox ID="txtVehicleRegNo" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="lblVehicleType" Font-Bold="true" runat="server">Vehicle Type: Private(P)/Commercial(C):</asp:Label>
                    <asp:RadioButtonList ID="rblVehicleType" CssClass="radio-inline" runat="server">
                        <asp:ListItem>P&nbsp</asp:ListItem>
                        <asp:ListItem>C</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <br />
            <br />
            <div class="row" align="center">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="btn btn-success btn-primary" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CssClass="btn btn-warning btn-primary" />                    
                </div>
                <div class="col-sm-4"></div>
            </div>
            <br />
            <div class="row" align="center">
                <div class="col-sm-4"></div>
                <div class="col-sm-2">
                    <asp:Button ID="btnFasTagRecharge" runat="server" Text="Go To FasTag Recharge" PostBackUrl="~/FastagRecharge.aspx" CssClass="btn alert-info btn-primary" />
                </div>
                <div class="col-sm-2">    
                    <asp:Button ID="btnVehicleTransaction" runat="server" Text="View Vehicle Transaction Entry" PostBackUrl="~/VehicleTransactionEntry.aspx" CssClass="btn alert-success btn-primary" />                   
                </div>
                <div class="col-sm-4"></div>
            </div>
            <br />
            <div class="box-body table-responsive">
                <asp:GridView ID="gvFasTag" Align="Center" HeaderStyle-BackColor="OliveDrab" HeaderStyle-BorderColor="#6600cc" HeaderStyle-ForeColor="#ffcc99"
                    runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" CssClass="table table-bordered table-striped gvfilters"
                    DataKeyNames="ID" OnRowDataBound="gvFasTag_RowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                    OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" Text="No records has been added.">
                    <EditRowStyle CssClass="GridViewEditRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No">
                            <ItemTemplate>
                                <asp:Label ID="lblSINO" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fastag ID">
                            <ItemTemplate>
                                <asp:Label ID="lblFastagID" runat="server" Text='<%#ProcessMyDataItem(Eval("NVAR_FASTAG_ID")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditFastagID" runat="server" Text='<%#ProcessMyDataItem(Eval("NVAR_FASTAG_ID")) %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Owner Name">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblOwnerName" Text='<%#ProcessMyDataItem(Eval("NVAR_OWNER_NAME")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditOwnerName" runat="server" Text='<%#ProcessMyDataItem(Eval("NVAR_OWNER_NAME")) %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vehicle Reg No">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblVehicleRegNo" Text='<%#ProcessMyDataItem(Eval("NVAR_VEHICLE_REG_NO")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditVehicleRegNo" runat="server" Text='<%#ProcessMyDataItem(Eval("NVAR_VEHICLE_REG_NO")) %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vehicle Type">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblVehicleType" Text='<%#ProcessMyDataItem(Eval("CHAR_VEHICLE_TYPE")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditVehicleType" runat="server" Text='<%#ProcessMyDataItem(Eval("CHAR_VEHICLE_TYPE")) %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Wallet Balance">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblWalletBalance" Text='<%#ProcessMyDataItem(Eval("INT_WALLET")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditWalletBalance" runat="server" Text='<%#ProcessMyDataItem(Eval("INT_WALLET")) %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Record Status">
                            <ItemTemplate>
                                <asp:Label ID="lblRtatus" runat="server" Text='<%#ProcessMyDataItem(Eval("CHAR_RECORDSTATUS")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" Text='<%#ProcessMyDataItem(Eval("ID")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Button ID="btnActive" runat="server" Text="Active" OnClick="btnActive_Click" CssClass="btn btn-success btn-xs" />
                                <asp:Button ID="btnInActive" runat="server" Text="In-Active" OnClick="btnInActive_Click" CssClass="btn btn-danger btn-xs" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" ShowEditButton="true" ShowDeleteButton="true" ControlStyle-CssClass="btn-group bg-danger"
                            HeaderText="Action" ControlStyle-Font-Bold="true" ControlStyle-ForeColor="Window" ControlStyle-BackColor="#cc66ff"
                            ItemStyle-Font-Bold="true" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </form>
</body>
</html>
