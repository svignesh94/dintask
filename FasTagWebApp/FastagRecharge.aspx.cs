﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Data.OleDb;
public partial class FastagRecharge : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
    DataProvider dp = new DataProvider();
    DataSet ds = new DataSet();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                txtFasTagID.Focus();
                LoadMainGrid();
                if (gvFasTag.DataSource == null)
                {
                    gvFasTag.DataSource = new string[] { };
                }
                gvFasTag.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public string ProcessMyDataItem(object myValue)
    {
        if (myValue.ToString() == "")
        {
            return "-";
        }
        return myValue.ToString();
    }
    protected void DisplayToastr(string message, string type)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "toastr", "alertMe(" + "\"" + message + "\"" + "," + "\"" + type + "\"" + ")", true);
    }
    protected void btnFasTagRecharge_Click(object sender, EventArgs e)
    {
       
        try
        {
            if(txtFasTagID.Text != "" && ddlFasRechAmont.Text != "")
            {
                BLL objRechargeBLL = new BLL();
                objRechargeBLL.FastagID = txtFasTagID.Text.ToUpper().Trim();
                objRechargeBLL.RechargeAmount = Convert.ToDecimal(ddlFasRechAmont.Text.ToUpper().Trim());
                objRechargeBLL.Logtime = DateTime.Now;              

                BLP objRechargeBLP = new BLP();
                objRechargeBLP.RechargeFastagDetails(objRechargeBLL);
                dp.ClearTextBoxes(this.Page);
                DisplayToastr("Added Successfully", "Success");
                txtFasTagID.Focus();
                gvFasTag.EditIndex = -1;
                this.LoadMainGrid();
            }
            else
            {
                DisplayToastr("Enter All Mandatory Fields", "Warning");
            }               

        }
        catch(Exception ex)
        {
            throw ex;
        }
      
    }
    public void LoadMainGrid()
    {
        ds.Clear();
        string q = "select * from Fastage_Customer_Details where CHAR_RECORDSTATUS='A' ORDER BY NVAR_OWNER_NAME ASC";
        ds = dp.GetTableSet(q);
        gvFasTag.DataSource = ds;
        gvFasTag.DataBind();
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtFasTagID.Text = string.Empty;
        ddlFasRechAmont.ClearSelection();
    }
}